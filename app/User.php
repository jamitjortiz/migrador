<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
//use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    //use HasApiTokens;
    use Notifiable;
    use SoftDeletes;

    protected $table = 'usuario';

    const CREATED_AT = 'fecha_creacion';

    const UPDATED_AT = 'fecha_actualizacion';

    const DELETED_AT = 'fecha_eliminacion';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
        'contrasena',
        'admin',
    ];



}
