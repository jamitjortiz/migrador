<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IntranetSucursal extends \App\Modelo
{
    const CREATED_AT = 'fecha_creacion';

    const UPDATED_AT = 'fecha_actualizacion';
    protected $table = 'intranet_sucursal';
    protected $fillable = [     
        'nombre',
        'codigo'

    ];
}
