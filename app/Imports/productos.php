<?php

namespace App\Imports;

use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Carbon\Carbon;
use App\IntranetPresolicitudProducto;

class productos implements ToModel
{
    /**
     * @param array $row
     *
     * @return IntranetPresolicitudProducto|null
     */
    public function model(array $row)
    {
     

        return new IntranetPresolicitudProducto([
            'id_usuario'=>1,
            'nombre'=>$row[0]
         ]);
         
    }
} 