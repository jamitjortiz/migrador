<?php

namespace App\Imports;


use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Carbon\Carbon;
use App\User ;

class usuarios implements ToModel
{
    /**
     * @param array $row
     *
     * @return IntranetContacto|null
     */
    public function model(array $row)
    {
      

        return new User([
            'nombre'=>$row[0],
            'contrasena'=>bcrypt('1234'),
            'admin'=>false,
         ]);
         
    }
} 