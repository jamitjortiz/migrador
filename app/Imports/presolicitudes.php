<?php

namespace App\Imports;

use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Carbon\Carbon;
use App\IntranetPresolicitud;
use App\IntranetSucursal;
use App\IntranetCliente;
use App\User;
class presolicitudes implements ToModel
{
    /**
     * @param array $row
     *
     * @return IntranetPresolicitud|null
     */
    public function model(array $row)
    {
     $sucursal = null; 
     if($row[1] == 0){
         $sucursal = 1;
     }
     if($row[1] == 2){
        $sucursal = 2;
    }
    if($row[1] == 3){
        $sucursal = 3;
    }
    $cliente = IntranetCliente::where('nombre','=',$row[2])->first();
    $usuario = User::where('nombre','=',$row[13])->first();
        return new IntranetPresolicitud([
            'id_simi'=>$row[0],
            'id_sucursal'=> $sucursal,
            'id_cliente'=>$cliente->id,
            'id_usuario'=>$usuario->id,
            'id_producto'=>$row[3],
            'monto_solicitado'=>$row[4],
            'monto_asignado'=>$row[4],
            'fecha_asignacion'=>$row[5],
            'fecha_solicitud'=>$row[5],
            'tasa_interes'=>$row[6],
            'plazo_asignado'=>$row[7],
            'plazo_solicitado'=>$row[7],
            'moneda'=>$row[8],
            'forma_credito'=>$row[9],
            'estado_presolicitud'=>$row[10],
            'descripcion'=>$row[11],
            'direccion'=>$row[12],
      
         ]);
         
    }
} 