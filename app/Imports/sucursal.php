<?php

namespace App\Imports;


use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Carbon\Carbon;
use App\IntranetSucursal;

class sucursal implements ToModel
{
    /**
     * @param array $row
     *
     * @return IntranetContacto|null
     */
    public function model(array $row)
    {
      

        return new IntranetSucursal([
             'codigo'=>$row[0],
             'nombre'=>$row[1]
         ]);
         
    }
} 