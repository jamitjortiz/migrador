<?php

namespace App\Imports;

use App\IntranetContacto;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Carbon\Carbon;
use App\IntranetCliente;

class contacts implements ToModel
{
    /**
     * @param array $row
     *
     * @return IntranetContacto|null
     */
    public function model(array $row)
    {
        $cliente = null ;
        if($row[1]== "EMPRESAS"){

            $cliente = IntranetCliente::where('ruc','=',$row[2])->first();
        }else{
            $cliente =  IntranetCliente::where('dni','=',$row[2])->first();

        }

        return new IntranetContacto([
             'id_cliente'=>$cliente->id,
             'tipo'=>'Telefono',
             'pertenece'=>$row[1] == "EMPRESAS"? "Empresa":"Persona",
             'descripcion'=>$row[3],
             'observacion'=>"ninguna",
             'status'=>1
         ]);
         
    }
} 