<?php
/**
 * Created by PhpStorm.
 * User: Alfredo Fleming
 * Date: 22/6/2019
 * Time: 11:37 AM
 */

namespace App;

class UsuarioUbicacion extends Modelo {

    public $timestamps = true;

    protected $table = 'usuario_ubicacion';

    /**
     * Los atributos que se pueden guardar
     *
     * @var array
     */
    protected $fillable = [
        'id_usuario',
        'latitud',
        'longitud',
        'telefono',
        'estado_bateria',
        'fecha_hora',
    ];


    /**
     * Devuélve las reglas de validación para un campo específico o el arreglo de reglas por defecto
     *
     * @param string $campo     Nombre del campo del que se quiere las reglas de validación.
     * @param int $ignorar_id    ID del elemento que se está editando, si es el caso.
     * @return array|string
     */
    public static function reglasValidacion($campo = null, $ignorar_id = 0) {
        $reglas = [
            'id_usuario'    => 'required|integer',
            'latitud'       => 'max:63',
            'longitud'      => 'max:63',
            'telefono'      => 'max:63',
            'estado_bateria'=> 'max:31',
            'fecha_hora'    => 'date_format:Y-m-d H:i:s',
        ];
        if ($campo === null) {
            return $reglas;
        }
        return isset($reglas[$campo]) ? $reglas[$campo] : '';
    }


    # RELACIONES


    # FILTROS


    # ASIGNACIONES


    # LECTURAS


    # METODOS

}