<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IntranetPresolicitudRole extends \App\Modelo
{
    const CREATED_AT = 'fecha_creacion';

    const UPDATED_AT = 'fecha_actualizacion';
    protected $table = 'intranet_user_rol_presolicitud';
    protected $fillable = [
        'id_presolicitud',
        'id_usuario',
        'role'
    ];
    
    
     
}
