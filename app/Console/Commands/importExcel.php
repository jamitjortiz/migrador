<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;
//me da ladilla ponerlas en mayusculas y esto se usara una sola vez
use App\Imports\clients;
use App\Imports\contacts;
use App\Imports\sucursal;
use App\Imports\productos;
use App\Imports\usuarios;
use App\Imports\presolicitudes;
use App\IntranetPresolicitud;
use App\IntranetPresolicitudRole;





class importExcel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'importacion de presolicitudes ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("inicializado proceso ");
        Excel::import(new clients, public_path().'/clientes.csv',null, \Maatwebsite\Excel\Excel::CSV);
        Excel::import(new contacts, public_path().'/clientes.csv',null, \Maatwebsite\Excel\Excel::CSV);
        Excel::import(new sucursal, public_path().'/sucursal.csv',null, \Maatwebsite\Excel\Excel::CSV);
        Excel::import(new productos, public_path().'/productos.csv',null, \Maatwebsite\Excel\Excel::CSV);
        Excel::import(new usuarios, public_path().'/users.csv',null, \Maatwebsite\Excel\Excel::CSV);
        Excel::import(new presolicitudes, public_path().'/presolicitudes.csv',null, \Maatwebsite\Excel\Excel::CSV);
        $presolicitudes = IntranetPresolicitud::all();
        foreach ($presolicitudes as  $presolicitud) {
             IntranetPresolicitudRole::create([
                'id_presolicitud'=>$presolicitud->id,
                'id_usuario'=>$presolicitud->id_usuario,
                'role'=>2
            ]);
             IntranetPresolicitudRole::create([
                'id_presolicitud'=>$presolicitud->id,
                'id_usuario'=>$presolicitud->id_usuario,
                'role'=>1
            ]);
        }      
        $this->info("fin del proceso");

    }

}
